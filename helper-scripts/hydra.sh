#!/bin/sh

# grep is used to highlight the supported services before running the wizard
hydra | grep --color=auto '^\|Supported services:'
hydra-wizard
