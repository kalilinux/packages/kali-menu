#!/bin/sh

echo
cd /usr/bin/
echo "    Passing-the-Hash binaries:"
echo "    --------------------------"
ls --color=auto pth-*
