#!/bin/sh

set -eu

### Helpers

file_exists() {
    if test -f $1; then return; fi
    echo "File $1 does not exist, or not a regular file" >&2
    exit 1
}

file_does_not_exist() {
    if test -e $1; then
        echo "File $1 already exists" >&2
        exit 1
    fi
}

files_are_identical() {
    file_exists $1
    file_exists $2
    if cmp $1 $2; then return; fi
    diff $1 $2 >&2
    exit 1
}

files_differ() {
    file_exists $1
    file_exists $2
    if cmp $1 $2; then
        echo "Files $1 and $2 are identical" >&2
        exit 1
    fi	
}

pkg_is_not_installed() {
    if dpkg -s $1 2>/dev/null | grep -q "ok installed"; then
        echo "Package $1 is already installed" >&2
        exit 1
    fi
}

### Warm up

apt-get update

#
### Test 1. A package that doesn't provide a desktop file
#

PKG=tcpdump
DESKTOP_FILE=/usr/share/applications/kali-$PKG.desktop
KALI_DESKTOP_FILE=/usr/share/kali-menu/applications/kali-$PKG.desktop
OVERRIDE_FILE=/etc/kali-menu/applications/kali-$PKG.desktop

# Basic assumptions before starting the test
file_exists $KALI_DESKTOP_FILE
pkg_is_not_installed $PKG
file_does_not_exist $DESKTOP_FILE

# After pkg installation, Kali's desktop file is installed
apt-get install -qq -y $PKG
file_exists $DESKTOP_FILE
files_are_identical $DESKTOP_FILE $KALI_DESKTOP_FILE

# Disable Kali's desktop file via override
ln -s /dev/null $OVERRIDE_FILE
/usr/share/kali-menu/update-kali-menu
file_does_not_exist $DESKTOP_FILE

# Remove override, thus re-enabling Kali's desktop file
rm -f $OVERRIDE_FILE
/usr/share/kali-menu/update-kali-menu
file_exists $DESKTOP_FILE

# After pkg removal, no more desktop file
apt-get purge -qq -y $PKG
file_does_not_exist $DESKTOP_FILE

#
### Test 2. A package that provides a desktop file
#

PKG=minicom
DESKTOP_FILE=/usr/share/applications/$PKG.desktop
KALI_DESKTOP_FILE=/usr/share/kali-menu/applications/$PKG.desktop
OVERRIDE_FILE=/etc/kali-menu/applications/$PKG.desktop

# Basic assumptions before starting the test
file_exists $KALI_DESKTOP_FILE
pkg_is_not_installed $PKG
file_does_not_exist $DESKTOP_FILE

# After pkg installation, Kali's desktop file is installed
apt-get install -qq -y $PKG
file_exists $DESKTOP_FILE
files_are_identical $DESKTOP_FILE $KALI_DESKTOP_FILE

# Disable Kali's desktop file via override
ln -s /dev/null $OVERRIDE_FILE
/usr/share/kali-menu/update-kali-menu
file_exists $DESKTOP_FILE
files_differ $DESKTOP_FILE $KALI_DESKTOP_FILE

# Remove override, thus re-enabling Kali's desktop file
rm -f $OVERRIDE_FILE
/usr/share/kali-menu/update-kali-menu
file_exists $DESKTOP_FILE
files_are_identical $DESKTOP_FILE $KALI_DESKTOP_FILE

# After pkg removal, no more desktop file
apt-get purge -qq -y $PKG
file_does_not_exist $DESKTOP_FILE

#
### Test 3. A "installer" desktop file
#

PKG=maltego
DESKTOP_FILE=/usr/share/applications/kali-$PKG.desktop
INSTALLER_FILE=/usr/share/applications/kali-$PKG-installer.desktop
KALI_DESKTOP_FILE=/usr/share/kali-menu/applications/kali-$PKG.desktop
KALI_INSTALLER_FILE=/usr/share/kali-menu/applications/kali-$PKG-installer.desktop
OVERRIDE_FILE=/etc/kali-menu/applications/kali-$PKG-installer.desktop

# Basic assumptions before starting the test
file_exists $KALI_DESKTOP_FILE
file_exists $KALI_INSTALLER_FILE
pkg_is_not_installed $PKG
file_does_not_exist $DESKTOP_FILE
file_exists $INSTALLER_FILE
files_are_identical $INSTALLER_FILE $KALI_INSTALLER_FILE

# Disable Kali's installer file via override
ln -s /dev/null $OVERRIDE_FILE
/usr/share/kali-menu/update-kali-menu
file_does_not_exist $DESKTOP_FILE
file_does_not_exist $INSTALLER_FILE

# Remove override, thus re-enabling Kali's installer file
rm -f $OVERRIDE_FILE
/usr/share/kali-menu/update-kali-menu
file_does_not_exist $DESKTOP_FILE
file_exists $INSTALLER_FILE

# After pkg installation, Kali's desktop file is installed,
# and Kali's installer file is removed
apt-get install -qq -y $PKG
file_does_not_exist $INSTALLER_FILE
file_exists $DESKTOP_FILE
files_are_identical $DESKTOP_FILE $KALI_DESKTOP_FILE

OVERRIDE_FILE=/etc/kali-menu/applications/kali-$PKG.desktop

# Disable Kali's desktop file via override
ln -s /dev/null $OVERRIDE_FILE
/usr/share/kali-menu/update-kali-menu
file_does_not_exist $DESKTOP_FILE
file_does_not_exist $INSTALLER_FILE

# Remove override, thus re-enabling Kali's desktop file
rm -f $OVERRIDE_FILE
/usr/share/kali-menu/update-kali-menu
file_exists $DESKTOP_FILE
file_does_not_exist $INSTALLER_FILE

# After pkg removal, no more desktop file,
# and Kali's installer desktop file is back
apt-get purge -qq -y $PKG
file_does_not_exist $DESKTOP_FILE
file_exists $INSTALLER_FILE
files_are_identical $INSTALLER_FILE $KALI_INSTALLER_FILE
